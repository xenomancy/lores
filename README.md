# Welcome to the Xenomancy Project Lore Repository!

This repository serves as a central hub for all the lore documents associated with the Xenomancy project.
Here, you'll find a meticulously crafted world waiting to be explored, documented entirely in markdown format.

We've categorized the lore into two distinct directories:

1. **Articles:**
   Dive deep into the intricate details of the Xenomancy universe through written explanations.
   These articles explore a vast array of in-universe concepts, from fascinating locations and captivating characters to the very fabric of reality within Xenomancy.
2. **Media:**
   Breathe life into the written word with the accompanying media section.
   Visualize the world through illustrations, gain a deeper understanding through demonstrations, and explore other creative formats that complement the articles.

For those seeking to delve into captivating narratives set within this world, we've got you covered! Head over to the [**Xenomancy Stories Repository**](https://gitlab.com/xenomancy/stories) to explore a collection of stories that unfold within the Xenomancy universe.
Feel free to explore both repositories to fully immerse yourself in the rich world of Xenomancy.
