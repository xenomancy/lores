---
title: Xenomancy Lores
bookFlatSection: false
bookCollapseSection: false
type: docs
---

# Welcome to the Xenomancy Project Lore Repository!

This repository serves as a central hub for all the lore documents associated with the Xenomancy project.
Here, you'll find a meticulously crafted world waiting to be explored, documented entirely in a website format.

Dive deep into the intricate details of the Xenomancy universe through articles accessible in this site.
These articles explore a vast array of in-universe concepts, from fascinating locations and captivating characters to the very fabric of reality within Xenomancy.

For those seeking to delve into captivating narratives set within this world, we've got you covered! Head over to the [**Xenomancy Stories Repository**](https://gitlab.com/xenomancy/stories) to explore a collection of stories that unfold within the Xenomancy universe.
Feel free to explore both repositories to fully immerse yourself in the rich world of Xenomancy.

