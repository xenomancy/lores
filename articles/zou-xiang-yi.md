---
title: Zou Xiang-Yi (邹翔毅)
author: Hendrik Lie
date: 2024-05-04T02:49:00+07:00
tags:
  - character
  - poi/dan
  - poi/xyz
---

# Zou Xiang-Yi (邹翔毅)

**Name and Title:** Dr. Zou Xiang-Yi, MD, MS, PhD

**Online Alias:** XYZ

**Gender:** Male

**Education:**

- Doctor of Philosophy (PhD) degree in musculoskeletal biomechanics. It is the doctoral degree he has earned in the specific field of musculoskeletal system and biomechanics.
- Master of Science (MS) in Biomedical Engineering. This provides advanced training in the engineering principles and technologies relevant to the musculoskeletal system.
- Doctor of Medicine (MD), indicates his medical degree.

**Profession:** Medical Doctor, Professor of Biomechanics and Musculoskeletal Systems

**Date of Birth:** April 24, 1986

**City of Birth:** Singapore

**Nationality:** Singaporean (formerly), later acquires his Indonesian nationality (from his Indonesian mother) when he turned 18 years old at 2004.

## Nature and Personality

Zou Xiang-Yi is born in Singapore from a Singaporean nationality father, and an Indonesian nationality mother.
He moved to Indonesia with his family after he graduated from junior high school.
Zou Xiang-Yi is a complex individual driven by a thirst for knowledge and a deep love for science fiction.
His analytical mind and focus on structure provide a foundation for his academic pursuits in medicine and his online persona as XYZ, a respected sci-fi worldbuilder.
However, this same focus can sometimes lead to social awkwardness and a tendency to interpret information literally.
Despite these challenges, Zou Xiang-Yi possesses a genuine empathy for others and a strong sense of loyalty, evident in his relationships with Fernando and Steven.

Shaped by early struggles to meet his parents' expectations, Zou Xiang-Yi forged his own path.
His unique medical condition, situs inversus totalis, sparked his fascination with biomechanics and became a defining aspect of his life.
Living in Indonesia during his younger years, he embraced the early internet, establishing his online identity (XYZ) that would become a cornerstone of his creative expression.
One of his endearing quirks is his tendency to workastinate, particularly when important exams approach. He escapes into writing sci-fi articles, requiring playful intervention from Fernando and Steven to ensure he prioritizes his studies.

Zou Xiang-Yi's literal interpretations and focus on established scientific principles create a hilarious misunderstanding in his friendship with Steven.
Unaware of Steven's half-alien nature, Zou Xiang-Yi attributes Steven's accurate yet fantastical insights to an overactive sci-fi imagination.
Ironically, these "misinterpretations" provide valuable inspiration for Zou Xiang-Yi's writing, unknowingly incorporating real-world alien knowledge into his fictional worlds.
This ongoing misunderstanding presents an opportunity for future growth as Zou Xiang-Yi confronts the limitations of his current understanding and embraces the fantastical possibilities that Steven embodies.

XYZ, Zou Xiang-Yi's online persona, is a well-respected author in sci-fi circles.
His detailed worldbuilding and creative exploration of scientific concepts have earned him recognition and a sense of community.
However, this online fame presents a challenge in balancing his real-life responsibilities.
His dedication to his sci-fi writing can lead to procrastination, requiring gentle nudges from Fernando and Steven to ensure he focuses on his medical studies.
Despite the occasional challenges, his online persona serves as a creative outlet and a source of inspiration for his work.

## Major Achievements

- Established himself as a well-respected author (XYZ) in online science fiction circles, known for his detailed worldbuilding and creative exploration of scientific concepts.
- Achieved academic success, graduating with medical honors and pursuing postgraduate and doctoral degrees in biomechanics and the musculoskeletal system.
- Became a distinguished professor at various universities in East Java, Indonesia.
- Contributed significantly to the advancement of humanoid robotics through his work with the non-profit KukerKolega Initiative.
  As a biomechanics consultant and designer, he played a key role in developing a robotic frame that mimics natural human movement with a high range of motion.

## Current Works (Professional Field)

Dr. Zou Xiang-Yi is a leading expert in biomechanics and musculoskeletal health.
His current research focuses on:

- Bioprinting and Musculoskeletal Tissue Engineering:

    - Development of Bioprinted Scaffolds for Cartilage Repair: Optimizing Biomechanical Properties and Cellular Differentiation
    - The Impact of Printing Parameters on the Vascularization and Functionality of Bioprinted Muscle Tissue

- Prosthetic and Orthotic Design:

    - Integrating Sensor Technology into Bioprinted Prosthetics for Real-Time Gait Monitoring and Feedback
    - Investigating the Use of Biocompatible and Lightweight Materials in 3D-Printed Prosthetics for Enhanced User Comfort and Performance

- Biomaterial Science and Musculoskeletal Health:

    - Development of Biodegradable Materials for Bone Implants: Balancing Strength, Resorption Rate, and Osteoconductive Properties
    - Investigating the Biocompatibility and Long-Term Performance of Novel 3D-Printed Materials for Cartilage Repair

## Hobby (XYZ persona)

Dr. Zou Xiang-Yi maintains a prolific online presence as XYZ, a respected author in the science fiction community. His current writing explores thought-provoking topics such as:

- The implications of causality if faster-than-light travel through a jumpdrive is possible, specifically focusing on the travel along the hypersurface of the present.
- Constructing a wormhole network with careful arrangements to prevent closed timelike curves, a paradox that could arise with time travel.
- Exploring the concept of transmutation in a metal-poor star system through the utilization of accretion disks on artificial mini black holes.

## Significance

Dr. Zou Xiang-Yi's story exemplifies how individuals on the autism spectrum can excel in their chosen fields while navigating social interactions.
He demonstrates the power of online communities to foster creativity and connection, and how science fiction can inspire real-world scientific advancements in biomechanics and medicine.

## Meaning of Name and Online Alias

Dr. Zou Xiang-Yi's (邹翔毅) name holds a deeper meaning that resonates with his personality and online persona (XYZ).

- **Zou (邹) - "to wander, roam"**:
  This character reflects Dr. Zou Xiang-Yi's inherent curiosity and his desire to explore the unknown.
  While he might crave structure in his daily life, his mind craves the boundless possibilities present in science fiction.
  The online world, with its vast array of ideas and communities, becomes a natural extension of this wanderlust.
    
- **Xiang (翔) - "soar, fly"**:
  This character embodies Dr. Zou Xiang-Yi's ambition and his yearning to push boundaries.
  He strives for excellence in his medical studies and allows his imagination to take flight in his sci-fi writing.
  The online persona, XYZ, becomes a platform for his ideas to soar and reach a wider audience.
    
- **Yi (毅) - "willpower, perseverance"**:
  This character highlights Dr. Zou Xiang-Yi's determination and unwavering focus.
  He dedicates himself to his passions, whether it's mastering complex medical concepts or meticulously crafting intricate sci-fi worlds.
  This perseverance is evident in his online presence, where he has established himself as a respected author.

During his time in Indonesia, young Xiang-Yi embraced the burgeoning online world.
Given the name order difference (first name last name) in Indonesian registry compared to the Chinese format, his online identity would reflect "Xiang-Yi Zou," resulting in the initials XYZ.
Perhaps he found a certain elegance in the simplicity of these initials, or maybe it was a playful nod to the unknown variables he explored in his sci-fi writing.
Whatever the reason, XYZ became his online persona, a vessel for his creativity and a way to connect with a global community that shared his love for science fiction.

## Autistic Traits and Coping Mechanism

Zou Xiang-Yi's world thrives on structure and predictability.
This characteristic, while a strength in many ways, aligns with potential traits of autism spectrum disorder.
Social interactions can be challenging for him.
He often struggles to interpret nonverbal cues and may appear overly literal or blunt in his communication.
To cope, Zou Xiang-Yi relies on routines and rituals. His online persona, XYZ, provides a safe space for creative expression and connection with like-minded individuals.
Additionally, his fascination with Steven's car becomes a unique social interaction for him.
By washing and talking to the car, he establishes a predictable and controlled social outlet, allowing him to connect with another "being" without the complexities of human interaction.

## Situs Inversus Totalis

Zou Xiang-Yi's medical condition, situs inversus totalis, where his internal organs are mirrored, has had a significant impact on his life.
While presenting insignificant challenges, it played a crucial role in saving his life.
During a random shooting incident, the atypical positioning of his organs meant the bullet missed vital areas.
This life-altering event likely fueled his fascination with biomechanics and the intricate workings of the human body.
He might see his own body as a testament to the body's remarkable resilience and a constant reminder of the delicate balance that sustains life.
Perhaps this experience also instilled in him a deeper appreciation for order and structure, a way of asserting control in a world that sometimes feels unpredictable.

## Personal Relationships

Dr. Zou Xiang-Yi's life isn't solely defined by his academic achievements and online presence.
He cherishes his personal connections, finding support and understanding in his inner circle.

- **Fernando Suryantara (Boyfriend):**
  A supportive and patient partner, Fernando acts as a calming influence for Dr. Zou Xiang-Yi.
  He understands Dr. Zou Xiang-Yi's unique personality and challenges, particularly his need for structure and routine.
  Fernando helps him maintain a healthy balance between his professional and personal life, ensuring Dr. Zou Xiang-Yi prioritizes his well-being.
  Their relationship provides a foundation of emotional security and allows Dr. Zou Xiang-Yi to navigate social interactions with greater confidence.
- **Steven Pontirijaris (Best Friend):**
  A half-alien disguised as a human medical student, Steven shares Dr. Zou Xiang-Yi's passion for science fiction.
  Their friendship flourishes despite an ongoing misunderstanding. Dr. Zou Xiang-Yi interprets Steven's accurate yet fantastical insights, fueled by his real-world alien experiences, as a sign of intense sci-fi fandom bordering on delusion.
  Ironically, these "misinterpretations" inspire Dr. Zou Xiang-Yi's writing, unknowingly incorporating elements of real-world alien knowledge.
  This unique dynamic creates a humorous situation with the potential for a deeper bond built on trust and shared experiences once the truth is revealed.
- **Adran (Steven's Car):**
  In an unexpected twist, Dr. Zou Xiang-Yi develops a unique social interaction with Steven's intelligent car, Adran.
  While others might find it odd, Dr. Zou Xiang-Yi finds comfort and stimulation in washing and talking to the car.
  Adran's artificial intelligence provides a predictable and non-judgmental "listener."
  He can discuss his ideas, concerns, and even frustrations with Adran, knowing it will receive his words objectively.
  This interaction allows him to process his emotions and explore his thoughts in a safe and controlled environment.
  Perhaps, on some level, the car's advanced technology resonates with his fascination with order and structure, creating a unique social connection that complements his online interactions as XYZ.

These relationships showcase different facets of Dr. Zou Xiang-Yi's personality. His love for Fernando highlights his desire for emotional connection and stability. His friendship with Steven, despite the misunderstanding, emphasizes his openness to new ideas and his ability to form strong bonds. The unique social interaction with Adran demonstrates his ability to find comfort and stimulation in unexpected ways. These connections enrich Dr. Zou Xiang-Yi's life and contribute to his overall well-being.



## Related Articles

- Situs Inversus Totalis
- Biomechanics and Musculoskeletal System
- Bioprinting and Tissue Engineering
- Prosthetic and Orthotic Design
- Science Fiction Worldbuilding
- Online Communities and Identity
- Autism Spectrum Disorder and Careers in Science
- KukerKolega Initiative (if applicable)

