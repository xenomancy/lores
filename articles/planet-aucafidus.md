---
title: Planet Aucafidus
author: Hendrik Lie
date: 2024-05-06T16:49:06+07:00
tags:
  - draft
  - aucafidus
  - worlds
  - alteration
---

# Planet Aucafidus

![Planet Aucafidus with visible city lights on the east coast of the Siyak Continent.](/media/PlanetAucafidus.png)

> Planet Aucafidus with visible city lights on the east coast of the Siyak Continent.

| **Aucafidus (Biculus)**         |                                                                                                                                                                                                                                                                                     |
| ------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Type**                        | Terrestrial Trojan Body                                                                                                                                                                                                                                                             |
| **Summary**                     | **Aucafidus** is a trojan planet located at the L4 Lagrange point of its companion gas giant, **Subralis** (HD 28185 b). Both planets share a common orbit around a main sequence star **HD 28185** (also known as HIP 20723 or SAO 149631), which is locally known as **Biculus**. |
| **Satellites**                  | none                                                                                                                                                                                                                                                                                |
| **Affiliation**                 | Multitude                                                                                                                                                                                                                                                                           |
| **Colonized**                   | ~250kya (from geological records)                                                                                                                                                                                                                                                   |
| **Population**                  | 25 billion entities (5 billion on the surface, 10 billion on the orbital bands, 10 billion infomorphs)                                                                                                                                                                              |
| **Orbital characteristics**     |                                                                                                                                                                                                                                                                                     |
| **Semimajor axis**              | 1.031 ± 0.060 AU                                                                                                                                                                                                                                                                    |
| **Eccentricity**                | 0.070 ± 0.040                                                                                                                                                                                                                                                                       |
| **Periastron**                  | 0.956 AU                                                                                                                                                                                                                                                                            |
| **Apastron**                    | 1.102 AU                                                                                                                                                                                                                                                                            |
| **Inclination**                 | 0 degree                                                                                                                                                                                                                                                                            |
| **Period of revolution**        | 1.0484 earth years; 382.928 earth days; 407.14 aucafidian days                                                                                                                                                                                                                      |
| **Synodic rotation period**     | 81261.94 seconds, 22:34:22, 0.941 earth days                                                                                                                                                                                                                                        |
| **Siderial rotation period**    | 81062.84 seconds, 22:31:03, 0.938 earth days                                                                                                                                                                                                                                        |
| **Axial tilt**                  | 24.6 degrees                                                                                                                                                                                                                                                                        |
| **Physical characteristics**    |                                                                                                                                                                                                                                                                                     |
| **Radius**                      | 5734 km                                                                                                                                                                                                                                                                             |
| **Mass**                        | 6.1213E+24 kg                                                                                                                                                                                                                                                                       |
| **Density**                     | 7.78 g/cm^3                                                                                                                                                                                                                                                                         |
| **Surface gravity**             | 12.43 m/s^2 (1.27 g)                                                                                                                                                                                                                                                                |
| **Atmospheric characteristics** |                                                                                                                                                                                                                                                                                     |
| **Pressure**                    | 1.98 bar                                                                                                                                                                                                                                                                            |
| **Composition**                 | 18% (356 mbar) oxygen; 81.6% (1616 mbar) nitrogen; 0.4% (8 mbar) other gases.                                                                                                                                                                                                       |

## Description

**Aucafidus** is a trojan planet located at the L4 Lagrange point of its companion gas giant, **Subralis** (HD 28185 b).
Both planets share a common orbit around a main sequence star **HD 28185** (also known as HIP 20723 or SAO 149631), which is locally known as **Biculus**.

Aucafidus boasts an exceptionally large iron-nickel core, constituting nearly half its total volume.
This substantial core generates a sufficiently robust magnetic field, a crucial factor for a planet harboring life.
As a consequence of this large core, Aucafidus possesses a slightly greater mass and significantly higher density compared to Earth.
Despite its smaller size, this translates to a surface gravity of 12.43 m/s², considerably stronger than Earth's.

The planet is home to an estimated 25 billion sentient beings residing in roughly 2 million cities concentrated on its southern continents.
The surface environment bears a strong resemblance to Earth, but with key differences.
Notably, the surface gravity is 1.27 times that of Earth, and the atmospheric pressure is nearly double.
The absence of a natural satellite further distinguishes Aucafidus from planet Earth.

## Surface Population

![Planet Aucafidus surface map, depicting the northern and southern continents.](/media/aucafidus-surface.png)

> Planet Aucafidus surface map, depicting the northern and southern continents.

Aucafidus, a planet teeming with life, boasts a surface population of roughly 25 billion sentient beings.
This populace is divided and distributed across two distinct continental regions, each with its own unique character and societal priorities.
Understanding this geographic separation is crucial to grasping the multifaceted nature of Aucafidus' surface society.

The southern continent of Aucafidus pulsates with the energy of a thriving civilization.
Here, sprawling urban centers dominate the landscape, boasting gleaming towers of metal and glass seamlessly integrated with verdant pockets of vegetation.
This reflects a societal commitment to both technological advancement and environmental consciousness.
Public transportation networks weave through the cities, minimizing reliance on personal vehicles and maintaining clean air.
The southern continent serves as the administrative and cultural heart of Aucafidus, housing public administrative districts and acting as a launchpad for travel to the orbital bands.

In stark contrast to the urban sprawl of the south, the northern continent of Aucafidus serves as a vast wildlife reserve.
Here, nature reigns supreme.
Towering mountain ranges pierce the sky, their slopes carpeted with lush evergreen forests.
Vast plains teem with diverse wildlife, while crystal-clear rivers snake through the landscape.
Sentient population on the northern continent is carefully managed, with limited outposts catering to scientific research and eco-tourism endeavors.
This deliberate approach ensures the preservation of the continent's pristine wilderness and its unique ecological treasures.

![Planet Aucafidus night lights, emphasizing an almost complete lack of urbanization on the northern continent.](/media/aucafidus-night.png)

> Planet Aucafidus night lights, emphasizing an almost complete lack of urbanization on the northern continent.

This distinct separation between the two continents reflects a deliberate societal choice.
The southern continent prioritizes progress and development, while the northern continent serves as a sanctuary for the planet's natural heritage.
Together, these two contrasting landscapes paint a vivid picture of Aucafidus' multifaceted society – a place where technological prowess exists in harmony with a deep respect for the natural world.

### The Northern Continent

The northern continent of Aucafidus stands in stark contrast to its southern counterpart.
Here, amidst a breathtaking expanse of wilderness, lies a testament to Aucafidus' commitment to environmental conservation.
Untouched by the sprawl of urban development, the north serves as a vital wildlife reserve, a sanctuary for the planet's diverse flora and fauna.

Towering mountain ranges pierce the sky, their slopes adorned with lush evergreen forests.
Vast plains teem with life, from herds of herbivores grazing on verdant grasslands to apex predators stalking their prey.
Crystal-clear rivers snake through the landscape, emptying into shimmering lakes that reflect the pristine beauty of the environment.

Sentient population on the northern continent is carefully managed.
Sparsely populated areas exist along the southern tip and coasts, primarily catering to **research facilities** and eco-tourism expeditions.
These outposts are designed with minimal environmental impact, ensuring the wilderness remains undisturbed.
Access to the continent's interior is strictly regulated, with **scientific research** serving as the primary justification for venturing deep into the wilds.

The northern continent serves as a vital resource for Aucafidus' scientific community.
Researchers flock here to study the planet's **ecology, biodiversity, and evolutionary history**.
The pristine environment offers a unique opportunity to observe the delicate balance of nature, providing invaluable insights for sustainable practices across the planet.

The northern continent embodies Aucafidus' commitment to **environmental responsibility**.
It serves as a reminder of the importance of preserving natural habitats and safeguarding the planet's biodiversity.
By protecting this pristine wilderness, Aucafidus ensures a healthy ecosystem for future generations and a source of wonder and scientific exploration for all.

### The Southern Continent

In contrast to the tranquil embrace of the northern continent, Aucafidus' southern landmass throbs with the vibrant energy of a bustling civilization.
Here, sprawling **urban centers** seamlessly integrate with pockets of verdant vegetation, creating a harmonious blend of technological prowess and environmental consciousness.

The southern continent is a mosaic of **highly-developed cityscapes**.
Towering structures of gleaming metal and glass pierce the sky, their design incorporating lush rooftop gardens and vertical farms.
Efficient public transportation networks weave through the cities, minimizing reliance on personal vehicles and maintaining clean air.

Aucafidus' southern continent prioritizes **clean energy**.
Solar power obtained from the First Band serves as the primary source of energy, ensuring minimal environmental impact.
This reliance on renewable energy reflects the society's commitment to a sustainable future.

The southern continent pulsates with life.
Bustling **amusement parks** offer thrills and entertainment for all ages.
Competitive and cooperative **games**, both physical and virtual, provide a platform for social interaction and friendly rivalry.
World-class **academic institutions and art centers** foster intellectual and artistic pursuits, with frequent exhibitions and performances showcasing the creative spirit of the citizenry.

The southern continent serves as the primary point of access to the orbital bands.
**Space tethers**, anchored in the equatorial region, provide a convenient and energy-efficient mode of transportation to the First Band.
This constant flow of immigration and exchange between the surface and the orbital habitats fosters a sense of interconnectedness and shared identity across Aucafidus.

The southern continent embodies Aucafidus' commitment to **urban development** while maintaining **environmental responsibility**.
The integration of nature within the cityscape highlights the society's respect for the natural world.
This blend of technological advancement, ecological awareness, and a thriving cultural scene makes the southern continent a beacon of progress and a testament to Aucafidus' ability to create a vibrant future for its people.

### Surface Governance

The surface population of Aucafidus presents a unique case study in societal organization.
This populace of approximately 25 billion sentient beings resides primarily on the southern continent, with a smaller population inhabiting the fringes of the northern continent.
Two key aspects define this society: a **decentralized governance structure** overseen by a single governing AI, and a strong emphasis on **citizen participation**.

Aucafidus' surface governance departs from traditional models.
A single, decentralized AI, mirroring the distributed decision-making style of the First Band, governs both continents.
This suggests a strong cultural and technological connection between the surface and the orbital habitats.
The governing AI leverages a network of **widespread avatars** stationed within public administrative offices throughout numerous districts.
This unique approach allows for direct interaction between the AI and the citizenry, fostering a sense of participation and trust within the population.

Public administration on the surface thrives on a spirit of volunteerism.
Individuals aspiring to public service roles undergo a rigorous **meritocratic selection process**, ensuring that only the most qualified candidates are entrusted with leadership positions within each district.
This selection process typically evaluates a candidate's educational background, relevant capabilities, and track record of service within the community.
Term limits, implemented with variations by district, help to prevent stagnation and ensure that fresh perspectives are continually introduced into the public administration.
This safeguards against the potential downsides of a meritocratic system, where long-term incumbents might become entrenched and resistant to new ideas.

A cornerstone of this system is **active citizen engagement**.
Public discourse is encouraged, with citizens having the ability to voice concerns and suggestions through public hearings within district offices.
The AI avatars can directly address these concerns during the hearings.
Additionally, a system exists for submitting **written feedback** through public administrative offices, which is then analyzed by the AI.
This feedback loop allows the governing AI to refine policies and ensure they remain aligned with the needs of the population.

The governing AI prioritizes transparency.
Policy decisions are accompanied by **readily available academic papers** explaining the underlying rationale.
This fosters trust and allows citizens to understand the reasoning behind the AI's actions.
Furthermore, the AI makes these papers accessible not only in technical languages but also through simplified summaries and visualizations.
This ensures that even citizens without a scientific background can grasp the core concepts behind the policies that affect their lives.
In essence, this commitment to transparency empowers the citizenry to participate more effectively in public discourse and hold the governing AI accountable for its decisions.

## Orbital Bands

Aucafidus, a vibrant world teeming with life, possesses a crown of awe-inspiring orbital megastructures.
Unlike planetary bodies adorned with natural satellites like moons or rings, Aucafidus boasts a remarkable collection of three orbital bands.
These colossal megastructures, marvels of engineering prowess, serve as both habitat and testament to a thriving civilization.

1. **The First Band**, tethered to the planetary surface, fosters a diverse biosphere.
   Here, a remarkable assemblage of intelligent beings coexists in a post-scarcity society.
   Humanoids share this artificial environment with cetaceans, corvids, and other genetically modified life forms.
   Their primary challenge lies in fostering understanding and collaboration across these vastly different species.

2. **The Second Band**, a marvel of high technology, traces a path perpendicular to the planet's orbital plane.
   Characterized by a high degree of technological advancement, every aspect of life within the Second Band is permeated by advanced technology.
   The population is a fascinating blend of humanoids, robots, alien species, and synthetic life forms.
   Benevolent AI administrators govern this society, prioritizing minimal conflict and maximizing the well-being of its inhabitants.
   Leisure activities within the Second Band primarily revolve around cooperative and competitive games.

3. **The Third Band**, shrouded in a perpetual twilight due to its alignment with the planet's terminator, it showcases a captivating display of etoan ingenuity.
   A network of strategically positioned mirror panels allows the inhabitants to simulate sunrises for both thermal regulation and timekeeping.
   This ingenious adaptation fosters a sustainable environment where bioluminescent life flourishes.
   Tourism serves as the primary source of income for the Third Band, with its inhabitants renowned for their hospitality and vibrant cultural expressions.

These three orbital bands, each with its distinct characteristics and narrative, offer a window into the remarkable diversity and ingenuity that defines Aucafidus and its inhabitants.
Further sections will delve deeper into each band, exploring their societies, cultures, and the fascinating challenges and opportunities they present.

### The First Band

The First Band, tethered to the equatorial line of the planet by a network of space elevators, hangs suspended in a silent ballet with the world below.
Its orbital plane is perpendicular with the planet's axis of rotation.
At 1.2 planetary radius away from the planet core, the band stretches 1146.8 kilometers wide, encompassing an area of 4.96 x 10^13 square meters.
Half of this expanse is dominated by shimmering open oceans.

Life thrives within this artificial ecosystem.
A surface acceleration of 8.63 m/s², slightly lower than Earth's, allows for a comfortable existence for its diverse inhabitants.
4.65 billion souls call this band home, with 3.1 billion inhabiting the vast, sprawling landmasses and 1.55 billion people call the ocean home, with some residing on numerous mobile platforms that navigate the gentle currents, while others choose to live directly in the open ocean.

First Band pulsates with the lives of a diverse, transplanted population.
Humanoids, alongside intelligent cetaceans, corvids, and other modified life forms, exist as a reserve population on a world not their own.
Though tethered to this foreign world, they remain subject to the guidance and support of the planet's caretakers.
In this post-scarcity society, resource scarcity and spatial limitations are no longer concerns.
However, the true challenge lies in fostering understanding and collaboration across vastly different species, ensuring a harmonious future for this unique mosaic of life within the First Band.

Unlike the Second Band's centralized AI governance, the First Band employs a fascinatingly decentralized approach.
The governing body, shrouded in anonymity, integrates with the population through avatars and proxies, gathering firsthand feedback.
Administrative duties are handled by autonomous cell groups of volunteers, while decision-making remains localized, fostering a sense of ownership and responsibility within this diverse society.
Thus, although appearing leaderless to external observers, the First Band thrives under a highly functional, decentralized governance model.
   
### The Second Band

The Second Band circles the planet, coplanar with its orbital plane.
Matching the planet's rotation, the Second Band experiences identical day and night cycles.
This band, positioned at a distance of 1.3 planetary radii from the core, boasts a width of 1089.46 kilometers.
Its total area, encompassing 5.1 x 10^13 square meters, mirrors the First Band's in size, half of which is dominated by vast, shimmering oceans.

Life thrives under the slightly lower surface acceleration of 7.35 m/s², which feels a touch lighter than Earth's gravity.
The Second Band is home to a bustling population of 9.57 billion souls.
Landmasses accommodate 6.38 billion, while 3.19 billion call the ocean home, residing on mobile platforms or in specially designed underwater habitats.

The landmasses serve as a haven of manicured landscapes, primarily comprising expansive parks, meticulously designed gardens, and bustling metropolitan areas.
Notably, the absence of wild fauna characterizes the Second Band's terrestrial environment.
Instead, domesticated pets and specially engineered creatures dedicated to environmental maintenance populate the surface.

The demographic makeup consists primarily of humanoids and robots.
However, a significant minority comprises alien species and synthetic life forms, underscoring the band's diverse population.
Notably, the Second Band thrives as a high-tech society.
Technological advancements permeate every aspect of life, evident in the ubiquitous presence of drones navigating the skies,
sophisticated information networks, and even extensive underwater optical and sonar communication systems.
Furthermore, the widespread use of universal fabricators has rendered the need for traditional material acquisition obsolete.

Governance within the Second Band is characterized by benevolent AI administrators,
prioritizing both conflict minimization and maximizing the comfort and well-being of the inhabitants.
Leisure activities largely revolve around cooperative and competitive games, fostering a sense of community and healthy competition within the society.
   
### The Third Band

The Third Band, distinct from the other bands, embraces a perpetual twilight existence.
Its orbital plane meticulously and actively aligned with the planet's terminator, the boundary between day and night.
This unique positioning ensures the band's surface is constantly bathed in a soft, ethereal glow, fostering a unique environment unlike the other bands.

Located at a distance of 1.5 planetary radii from the core, the Third Band stretches 917.44 kilometers wide, encompassing a total area of 4.96 x 10^13 square meters.
Similar to the other bands, half of this area is dominated by vast oceans.
The surface experiences a slightly lower gravity compared to Earth, with an acceleration of 5.52 m/s^2.

This band is home to a population of 2.32 billion souls.
Of these, 1.55 billion reside on the landmasses, while the remaining 768 million call the ocean home.
However, the Third Band's defining feature lies not in its population, but in its ingenious adaptation to the perpetual twilight.

To manage both thermal regulation and timekeeping, the Third Band utilizes a network of mirror panels embedded within its side walls.
These panels, acting as giant reflectors, can artificially project the sun's image up to ten degrees above the horizon at regular intervals,
mimicking the sun's daily cycle.
This ingenious solution not only provides much-needed warmth and a sense of normalcy for the inhabitants, but also facilitates the regulation of their daily lives.

The unique environment of the Third Band has trivially become its primary source of income.
Tourists flock to experience the ethereal beauty of the perpetual twilight and witness the breathtaking spectacle of the artificial sunrises.
The locals, known for their warm hospitality, take great pleasure in interacting with visitors,
offering them the opportunity to savor spicy local cuisines and partake in the vibrant nightlife attractions.

## Surface and Orbital Bands Population Interaction

The relationship between the surface population of Aucafidus and its three orbital bands – the First Band, Second Band, and Third Band – transcends mere co-existence.
It thrives on a foundation of **interdependence and exchange**, fostering a symbiotic relationship that benefits all inhabitants of the planetary system.

### Immigration and Cultural Exchange

A hallmark of this connection is the **frequent flow of immigration** between the surface and the bands.
The First Band, with its post-scarcity society, attracts surface dwellers seeking new opportunities or a different lifestyle.
Conversely, some band inhabitants might choose the grounded way of life offered by the surface.
This exchange of individuals fosters a **cross-pollination of ideas and cultural practices**, enriching both societies.
The resulting interweaving culture reflects the diverse experiences and perspectives of Aucafidus' population.

### Travel and Tourism

Physical separation does not impede interaction.
**Space tethers** anchored in the equatorial region provide a convenient and energy-efficient mode of transportation for travel between the surface and the First Band.
For journeys between the bands themselves, conventional spacecraft are utilized.
Tourism flourishes, with distinct attractions on each band:

- **First Band:** The allure of the First Band lies in its post-scarcity lifestyle.
  Surface dwellers can immerse themselves in a world of advanced technology, explore novel experiences, and engage with the vibrant cultural scene.
- **Second Band:** The intellectual stimulation offered by the Second Band's high-tech society, its various amusement parks, gardens, and entertainment centers, attracts adventurous and curious souls.
- **Third Band:** The perpetual twilight and bioluminescent life forms of the Third Band provide a tranquil escape for surface dwellers, offering an opportunity to reconnect with nature.

### Resource Exchange and Shared Infrastructure

The surface population leverages the **solar power** obtained from the First Band to meet their energy needs.
This clean and sustainable source of energy reflects the societal commitment to environmental responsibility.
Furthermore, the bands likely engage in the exchange of resources and manufactured goods.
The surface, with its vast natural resources, might provide raw materials for the bands' industries, while the bands, with their advanced technology, could export finished goods to the surface.
This exchange strengthens the economic interdependence between the surface and the bands.

### Collaborative Governance for a Unified Aucafidus

The governing AI on the surface likely maintains close communication with the governing AIs of the bands.
This collaboration ensures a **unified approach to governance** across all of Aucafidus.
By coordinating policies and resource allocation, the governing bodies can facilitate the smooth flow of resources and people between the surface and the bands.

### Fostering Unity and Progress

Despite the physical separation, the citizens of the surface and the bands share a common identity as Aucafidus citizens.
Regular cultural exchanges, educational programs, and sporting events foster a sense of **unity and shared purpose**.
The exchange of knowledge and experiences between the surface and the bands contributes to the continuous development and progress of Aucafidus as a whole.

In conclusion, the interactions between the surface population and the orbital bands extend far beyond mere contact.
They represent a **symbiotic relationship** built on exchange, collaboration, and a shared vision for the future.
This interconnectedness strengthens Aucafidus' societal fabric and propels the entire civilization forward.
